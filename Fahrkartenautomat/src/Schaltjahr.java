import java.util.Scanner; 
public class Schaltjahr {
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Bitte geben Sie ein Jahr ein: ");
		int Jahr = myScanner.nextInt();
		if ((Jahr > -45) && (Jahr %4 == 0) && ((Jahr %100 != 0) || (Jahr %400 == 0))){
			System.out.println("Das Jahr ist ein Schaltjahr");
		}
		else System.out.println("Das Jahr ist kein Schaltjahr");
	}
	
		
}


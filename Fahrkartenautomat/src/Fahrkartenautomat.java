﻿import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);

		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;
		double rückgabebetrag;
		byte ticketzahl;
		
		System.out.print("Anzahl der Tickets: ");
		ticketzahl = tastatur.nextByte();
		System.out.print("Zu zahlender Betrag pro Ticket (EURO): ");
		zuZahlenderBetrag = tastatur.nextDouble();
		zuZahlenderBetrag = zuZahlenderBetrag * ticketzahl;

		// Geldeinwurf
		// -----------
		eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: %.2f Euro \n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}

		// Fahrscheinausgabe
		// -----------------
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");

		// Rückgeldberechnung und -Ausgabe
		// -------------------------------
		rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (rückgabebetrag > 0.0) {
			System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rückgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
		
	}	
		//Die verwendeten Variabeln und deren Datentypen sind:
		//  Variabel						Datentyp
		//	zuZahlenderBetrag				double
		//	eingezahlterGesamtbetrag		double
		//	eingeworfeneMünze				double
		//	rückgabebetrag					double
		//	tastatur						Scanner
		//	i								integer
		//	ticketzahl						byte
		//
		//Es wurden folgende 6 Operatoren verwendet: =, ++, -, -=, ,, +
		//Ich habe mich für einen Byte für die Ticketzhl entschieden, weil ja nur ganze Tickets verkauft werden können und man daher keine Nachkommastellen benötigt. Außerdem kommt es unheimlich selten vor das jemand mehr als 127 Tickets gleichzeitig kauft, da an diesem Punkt monatskarten günstiger wären und die Fahrkarten nach einer bestimmten Zeit nach der nächste Ticketpreiserhöhung sowieso verfallen
}

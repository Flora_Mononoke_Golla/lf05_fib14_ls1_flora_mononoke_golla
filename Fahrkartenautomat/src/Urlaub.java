
import java.util.Scanner;
class Urlaub {
  public static void main(String[] args) {
    
    Scanner scan = new Scanner(System.in);
    
    int wechselland;
    char buchenJaNein;                                             
    double wechselmenge, fremdwaehrung, reisekasse, reserve;
    
    echoln("\n\n\n\nReisezentrum Wunderland\nIhre Begleitung in die Wunderwelt!\n\n\n");
    
    echo("Wie viel Geld nehmen Sie mit? (Angabe in EUR): ");
    reisekasse = scan.nextDouble();
    echo("Wie gro� soll die eiserne Reserve sein? (Angabe in EUR): ");
    reserve = scan.nextDouble();
    
    while (true) { 
      
      echoln("\nIn welchem Land m�chten Sie Ihre EUR in Fremdw�hrung umtauschen? W�hlen Sie...");
      echoln("'1' f�r USA.");
      echoln("'2' f�r Japan.");
      echoln("'3' f�r Gro�britanien.");
      echoln("'4' f�r Schweiz.");
      echoln("'5' f�r Schweden.");
      echo("Ihre Auswahl: ");
      wechselland = scan.nextInt();
      
      echoln("Wie viel EUR aus Ihrer Reisekasse m�chten Sie umtauschen?");
      echo("Ihre Auswahl: ");
      wechselmenge = scan.nextDouble();
      
      switch (wechselland) {
        case 1:
        fremdwaehrung = wechselDollar(wechselmenge);
        break;
        case 2:
        fremdwaehrung = wechselYen(wechselmenge);
        break;
        case 3:
        fremdwaehrung = wechselPfund(wechselmenge);
        break;
        case 4:
        fremdwaehrung = wechselSchweizerFranken(wechselmenge);
        break;
        case 5:
        fremdwaehrung = wechselSchwedischeKronen(wechselmenge);
        break;
        default:
        echoln("FEHLER: Nummer " + wechselland + " ist kein g�ltiges Land!");
        continue;
      }
      
      echoln("Sie erhalten " + fremdwaehrung + " " + waehrungseinheit(wechselland));
      
      echo("Jetzt auf Reisekasse buchen? (j/n) ");
      buchenJaNein = scan.next().charAt(0);
      if (buchenJaNein == 'j' || buchenJaNein == 'J') {
        if (reisekasse < wechselmenge) {
          echoln("\nFEHLER:\nBetrag kann nicht gebucht werden, es sind nicht genug Reserven in der Reisekasse vorhanden.");
          echoln("In der Reisekasse: " + reisekasse + " EUR");
          echoln("Angefragte Buchung: " + wechselmenge + " EUR");
          continue;
        } else {
          reisekasse = reisekasse - wechselmenge;
          echoln("\nIn der Reisekasse verbleiben " + reisekasse + " EUR.\n");
        }        
      } else {
        echoln("\nIn der Reisekasse verbleiben " + reisekasse + " EUR.\n");
        continue;
      }
      
      if (reisekasse < reserve) {
        echoln("\nEiserne Reserve unterschritten. In der Reisekasse verbleibend: " + reisekasse);
        echoln("Programm wird nun beendet.");
        break;
      }
    }
    
  }
  
  static double wechselDollar(double wechselmenge) {
    double dollar = wechselmenge * 1.22;
    return dollar;
  }
  
  static double wechselYen(double wechselmenge) {
    double yen = wechselmenge * 126.50;
    return yen;
  }
  
  static double wechselPfund(double wechselmenge) {
    double pfund = wechselmenge * 0.89;
    return pfund;
  }
  
  static double wechselSchweizerFranken(double wechselmenge) {
    double franken = wechselmenge * 1.08;
    return franken;
  }
  
  static double wechselSchwedischeKronen(double wechselmenge) {
    double kronen = wechselmenge * 10.10;
    return kronen;
  }
  
  static String waehrungseinheit(int wechselland) {
    switch (wechselland) {
      case 1:
      return "USD (US-Dollar)";
      case 2:
      return "JPY (Yen)";
      case 3:
      return "GBP (Pfund)";
      case 4:
      return "CHF (Schweizer Franken)";
      case 5:
      return "SEK (Swedische Kronen)";
    }
    return "FATAL ERROR.";
  }
  
  static void echo(String string) {
    System.out.print(string);
  }
  
  static void echoln(String string) {
    System.out.println(string);
  }
}

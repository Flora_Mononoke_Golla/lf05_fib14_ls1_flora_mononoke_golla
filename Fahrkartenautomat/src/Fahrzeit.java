import java.util.Scanner;

public class Fahrzeit {
    public static void main(String[] args) {
		int fahrzeit = 0;
		char haltInSpandau = 'n';
		char richtungHamburg = 'n';
		char haltInStendal = 'j';
		String endetIn = "Hannover";

		fahrzeit = fahrzeit + 8;
		if (haltInSpandau == 'j') {
            fahrzeit = fahrzeit + 2;
        }
        if (richtungHamburg == 'n') {
            fahrzeit = fahrzeit + 34;
        } 
        else {
            fahrzeit = fahrzeit + 96;
        }
        if (haltInStendal == 'j') {
            fahrzeit = fahrzeit + 16;
        }
        else {
            fahrzeit = fahrzeit + 6;
        }
        if (endetIn.equals ("Hannover")) {
            fahrzeit = fahrzeit + 63;
        }
        else if (endetIn.equals ("Braunschweig")) {
            fahrzeit = fahrzeit + 50;
        }
        else {
        	fahrzeit = fahrzeit + 29;
        }
        System.out.println("Sie erreichen " + endetIn + " in " + fahrzeit + " Minuten.");

	}
}
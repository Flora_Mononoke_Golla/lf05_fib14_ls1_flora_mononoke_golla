class Temperatur {

	public static void main(String[] args) {
		
		String a = "-";
		double b = -28.8889;
		double c = -23.3333;
		double d = -17.7778;
		double e = -6.6667;
		double f = -1.1111;
		
		System.out.printf( "%-11s %s %9s\n", "Fahrenheit", "|", "Celsius");
		System.out.printf( "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n", a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a);
		System.out.printf( "%-11s %s %9.2f\n", "-20", "|", b);
		System.out.printf( "%-11s %s %9.2f\n", "-10", "|", c);
		System.out.printf( "%-11s %s %9.2f\n", "+0", "|", d);
		System.out.printf( "%-11s %s %9.2f\n", "+20", "|", e);
		System.out.printf( "%-11s %s %9.2f\n", "+30", "|", f);
		
		// Ich habe jetzt nur 23 "-" gemacht und nicht 24 wie auf dem Beispielausgabe angegeben, weil ich mich an die Breite von 12 Stellen links und 10 Stellen rechts vom "|" gehalten habe.
	}
}



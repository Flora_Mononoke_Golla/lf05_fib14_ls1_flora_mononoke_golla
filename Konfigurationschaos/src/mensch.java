
import java.util.Scanner; // Import der Klasse Scanner 

public class mensch {
    public static void main(String[] args) // Hier startet das Programm
    {
        Scanner myScanner = new Scanner(System.in);
        System.out.print("Bitte geben Sie ihren Namen ein: ");
        String name = myScanner.nextLine();
        System.out.print("Bitte geben Sie ihr Alter ein: ");
        String alter = myScanner.nextLine();
        System.out.print("Bitte geben Sie ihren Wohnort ein: ");
        String wohnen = myScanner.nextLine();
        System.out.print("Bitte geben Sie ihre Lieblingsfarbe ein: ");
        String farbe = myScanner.nextLine();
        
        System.out.printf("\nHallo %s, du bist wirklich schon %s jahre alt, das sieht man dir ja gar nicht an. Du kommst also aus %s und deine Lieblings farbe ist anscheinend %s", name, alter, wohnen, farbe );
        
        
        myScanner.close();
    }
}

import java.util.Scanner;

class Ticketautomat2 {
    public Scanner tastatur = new Scanner(System.in);

    public static void main(String[] args) {
        new Ticketautomat2();
    }

    public Ticketautomat2() {
        // tastatur = new Scanner(System.in);
        double zuZahlenderBetrag = fahrkartenbestellungErfassen();
        double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
        fahrkartenAusgeben();
        rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
        System.out.println(
                "Vergessen Sie nicht, den Fahrschein vor Fahrtantritt entwerten zu lassen!\n");
        int neuesticket = 0;
        while (neuesticket != 1 && neuesticket != 2) {
            System.out.println("Moechten Sie noch eine weitere Fahrkarte erwerben?\n1 --- Ja\n2 --- NEIN");
            neuesticket = tastatur.nextInt();
        }
        if (neuesticket == 1) {
            new Ticketautomat();
            // System.out.println("ja");
        } else if (neuesticket == 2) {
            System.out.printf("%sWir w�nschen Ihnen noch eine gute Fahrt%s", Color.GREEN, Color.RESETALL);
        } else {
            System.out.println("Error code 223, wie hast du es aus der Schleife geschafft?");
        }

        tastatur.close();
    }

    public class Color {
        public static final String RESETALL = "\033[0m";
        public static final String RED = "\033[31m";
        public static final String GREEN = "\033[32m";
        public static final String YELLOW = "\033[33m";

    }

    public double fahrkartenbestellungErfassen() {
        double zuZahlenderBetrag = 0.0;
        System.out.print("Anzahl der Tickets: ");
        int ticketzahl = tastatur.nextInt();

        if (ticketzahl > 10) {
            System.out.printf("%sSo viele Tickets sind nicht erlaubt!%s\nDu kannst stattdessen 1 Ticket kaufen\n",
                    Color.RED, Color.RESETALL);
            ticketzahl = 1;
        } else if (ticketzahl < 1) {
            System.out.printf(
                    "%sNegative Anzahl Tickets sind nicht erlaubt!%s\nDu kannst stattdessen 1 Ticket kaufen\n",
                    Color.RED, Color.RESETALL);
            ticketzahl = 1;
        } else {
            System.out.printf("%sDu moechtest also %s Tickets%s\n", Color.GREEN, ticketzahl, Color.RESETALL);
        }
        while (zuZahlenderBetrag == 0) {
            System.out.print("Zu zahlender Betrag pro Ticket (EURO): ");
            // zuZahlenderBetrag *= tastatur.nextDouble();
            zuZahlenderBetrag = tastatur.nextDouble();

            zuZahlenderBetrag = zuZahlenderBetrag * ticketzahl;
        }
        return zuZahlenderBetrag;
    }

    public double fahrkartenBezahlen(double zuZahlenderBetrag) {
        double eingezahlterGesamtbetrag = 0.0;
        double eingeworfeneMuenze = 0.0;
        while ((eingezahlterGesamtbetrag < zuZahlenderBetrag) && (eingeworfeneMuenze == 0)) {
            double restbetrag = zuZahlenderBetrag - eingezahlterGesamtbetrag;
            System.out.printf("%sNoch zu zahlen: %.2f Euro %s\n", Color.GREEN, restbetrag, Color.RESETALL);
            System.out.print("Eingabe (mind. 5Ct, hoechstens 2 Euro): ");
            double eingeworfeneMuenze1 = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMuenze1;
        }
        return eingezahlterGesamtbetrag;
    }

    public void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    public void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
        double rueckgabewert = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if (rueckgabewert > 0.0) {
            System.out.println("Der Rueckgabebetrag in Hoehe von " + rueckgabewert + " EURO");
            System.out.println("wird in folgenden Muenzen ausgezahlt:");

            while (rueckgabewert >= 2.0) {
                // 2 EURO-Muenzen
                System.out.println("2 EURO");
                rueckgabewert -= 2.0;
            }
            if (rueckgabewert >= 1.0) {
                // 1 EURO-Muenzen
                System.out.println("1 EURO");
                rueckgabewert -= 1.0;
            }
            if (rueckgabewert >= 0.5) {
                // 50 CENT-Muenzen
                System.out.println("50 CENT");
                rueckgabewert -= 0.5;
            }
            while (rueckgabewert >= 0.2) {
                // 20 CENT-Muenzen
                System.out.println("20 CENT");
                rueckgabewert -= 0.2;
            }
            if (rueckgabewert >= 0.1) {
                // 10 CENT-Muenzen
                System.out.println("10 CENT");
                rueckgabewert -= 0.1;
            }
            if (rueckgabewert >= 0.05) {
                // 5 CENT-Muenzen
                System.out.println("5 CENT");
                rueckgabewert -= 0.05;
            }
        }
    }
}

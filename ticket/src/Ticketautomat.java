import java.util.Scanner;

class Ticketautomat {
    public Scanner tastatur = new Scanner(System.in);

    public static void main(String[] args) {
        new Ticketautomat();
    }

    public Ticketautomat() {
        // tastatur = new Scanner(System.in);
        double zuZahlenderBetrag = fahrkartenbestellungErfassen();
        double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
        fahrkartenAusgeben();
        rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
        System.out.println(
                "Vergessen Sie nicht, den Fahrschein vor Fahrtantritt entwerten zu lassen! \nWir wuenschen Ihnen eine gute Fahrt.");
        tastatur.close();
    }

    public double fahrkartenbestellungErfassen() {
        double zuZahlenderBetrag = 0.0;
        int ticketzahl = 0;
        while (ticketzahl % 1 != 0 || ticketzahl == 0) {
            System.out.print("Anzahl der Tickets: ");
            ticketzahl = tastatur.nextInt();
        }
        while (zuZahlenderBetrag == 0) {
            System.out.print("Zu zahlender Betrag pro Ticket (EURO): ");
            // zuZahlenderBetrag *= tastatur.nextDouble();
            zuZahlenderBetrag = tastatur.nextDouble();

            zuZahlenderBetrag = zuZahlenderBetrag * ticketzahl;
        }
        return zuZahlenderBetrag;
    }

    public double fahrkartenBezahlen(double zuZahlenderBetrag) {
        double eingezahlterGesamtbetrag = 0.0;
        double eingeworfeneMuenze = 0.0;
        while ((eingezahlterGesamtbetrag < zuZahlenderBetrag) && (eingeworfeneMuenze == 0)) {
            double restbetrag = zuZahlenderBetrag - eingezahlterGesamtbetrag;
            System.out.printf("Noch zu zahlen: %.2f Euro \n", restbetrag);
            System.out.print("Eingabe (mind. 5Ct, hoechstens 2 Euro): ");
            double eingeworfeneMuenze1 = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMuenze1;
        }
        return eingezahlterGesamtbetrag;
    }

    public void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    public void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
        double rueckgabewert = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if (rueckgabewert > 0.0) {
            System.out.println("Der Rueckgabebetrag in Hoehe von " + rueckgabewert + " EURO");
            System.out.println("wird in folgenden Muenzen ausgezahlt:");

            while (rueckgabewert >= 2.0) {
                // 2 EURO-Muenzen
                System.out.println("2 EURO");
                rueckgabewert -= 2.0;
            }
            if (rueckgabewert >= 1.0) {
                // 1 EURO-Muenzen
                System.out.println("1 EURO");
                rueckgabewert -= 1.0;
            }
            if (rueckgabewert >= 0.5) {
                // 50 CENT-Muenzen
                System.out.println("50 CENT");
                rueckgabewert -= 0.5;
            }
            while (rueckgabewert >= 0.2) {
                // 20 CENT-Muenzen
                System.out.println("20 CENT");
                rueckgabewert -= 0.2;
            }
            if (rueckgabewert >= 0.1) {
                // 10 CENT-Muenzen
                System.out.println("10 CENT");
                rueckgabewert -= 0.1;
            }
            if (rueckgabewert >= 0.05) {
                // 5 CENT-Muenzen
                System.out.println("5 CENT");
                rueckgabewert -= 0.05;
            }
        }
    }
}

import java.util.Scanner;

class Ticketautomat3 {
    public Scanner tastatur = new Scanner(System.in);

    public static void main(String[] args) {
        new Ticketautomat3();
    }

    public Ticketautomat3() {
        // tastatur = new Scanner(System.in);
        double zuZahlenderBetrag = fahrkartenbestellungErfassen();
        double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
        fahrkartenAusgeben();
        rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
        System.out.println(
                "Vergessen Sie nicht, den Fahrschein vor Fahrtantritt entwerten zu lassen! \nWir wuenschen Ihnen eine gute Fahrt.\n");
        int neuesticket = 0;

        while (neuesticket != 1 && neuesticket != 2) {
            System.out.println("Moechten Sie noch eine weitere Fahrkarte erwerben?\n1 --- Ja\n2 --- NEIN");
            neuesticket = tastatur.nextInt();
        }
        if (neuesticket == 1) {
            new Ticketautomat();
            // System.out.println("ja");
        } else if (neuesticket == 2) {
            System.out.printf("%sWir w�nschen Ihnen noch eine gute Fahrt%s", Color.GREEN, Color.RESETALL);
        } else {
            System.out.println("vielleicht");
        }

        tastatur.close();
    }

    public class Color {
        public static final String RESETALL = "\033[0m";
        public static final String RED = "\033[31m";
        public static final String GREEN = "\033[32m";
        public static final String YELLOW = "\033[33m";

    }

    public double fahrkartenbestellungErfassen() {
        double zuZahlenderBetrag = 0.0;
        int ticketart = 0;
        while (ticketart != 1 && ticketart != 2 && ticketart != 3) {
            System.out.println(
                    "W�hlen Sie Ihre Wunschfahrkarte f�r Berlin AB aus: \n1 --- Einzelfahrschein Regeltarif AB [3,00 EUR] \n2 --- Tageskarte Regeltarif AB [8,80 EUR] \n3 --- Kleingruppen-Tageskarte Regeltarif AB [25,50 EUR]\n");
            ticketart = tastatur.nextInt();
            if (ticketart != 1 && ticketart != 2 && ticketart != 3) {
                System.out.printf("%sung�ltige Eingabe\n%s", Color.RED, Color.RESETALL);
            }
        }
        if (ticketart == 1) {
            System.out.printf("%sDu willst also einen Einzelfahrschein\n%s", Color.GREEN, Color.RESETALL);
            zuZahlenderBetrag = 3;
        } else if (ticketart == 2) {
            System.out.printf("%sDu willst also eine Tageskarte\n%s", Color.GREEN, Color.RESETALL);
            zuZahlenderBetrag = 8.80;
        } else if (ticketart == 3) {
            System.out.printf("%sDu willst also eine Kleingruppen-Tageskarte\n%s", Color.GREEN, Color.RESETALL);
            zuZahlenderBetrag = 25.50;
        } else {
            System.out.println("Error code 223, wie hast du es aus der Schleife geschafft?\n");
        }

        System.out.print("Wie viele Tickets m�chtest du: ");
        int ticketzahl = tastatur.nextInt();
        while (ticketzahl < 1 || ticketzahl > 10) {
            if (ticketzahl < 1 || ticketzahl > 10) {
                System.out.print("Es sind nur 1 - 10 Tickets kaufbar!\n");
            }
            System.out.print("Anzahl der Tickets: ");
            ticketzahl = tastatur.nextInt();
        }
        zuZahlenderBetrag *= ticketzahl;
        return zuZahlenderBetrag;
    }

    public double fahrkartenBezahlen(double zuZahlenderBetrag) {
        double eingezahlterGesamtbetrag = 0.0;
        double eingeworfeneMuenze = 0.0;
        while ((eingezahlterGesamtbetrag < zuZahlenderBetrag) && (eingeworfeneMuenze == 0)) {
            double restbetrag = zuZahlenderBetrag - eingezahlterGesamtbetrag;
            System.out.printf("%sNoch zu zahlen: %.2f Euro %s\n", Color.GREEN, restbetrag, Color.RESETALL);
            System.out.print("Eingabe (mind. 5Ct, hoechstens 2 Euro): ");
            double eingeworfeneMuenze1 = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMuenze1;
        }
        return eingezahlterGesamtbetrag;
    }

    public void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    public void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
        double rueckgabewert = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if (rueckgabewert > 0.0) {
            System.out.println("Der Rueckgabebetrag in Hoehe von " + rueckgabewert + " EURO");
            System.out.println("wird in folgenden Muenzen ausgezahlt:");

            while (rueckgabewert >= 2.0) {
                // 2 EURO-Muenzen
                System.out.println("2 EURO");
                rueckgabewert -= 2.0;
            }
            if (rueckgabewert >= 1.0) {
                // 1 EURO-Muenzen
                System.out.println("1 EURO");
                rueckgabewert -= 1.0;
            }
            if (rueckgabewert >= 0.5) {
                // 50 CENT-Muenzen
                System.out.println("50 CENT");
                rueckgabewert -= 0.5;
            }
            while (rueckgabewert >= 0.2) {
                // 20 CENT-Muenzen
                System.out.println("20 CENT");
                rueckgabewert -= 0.2;
            }
            if (rueckgabewert >= 0.1) {
                // 10 CENT-Muenzen
                System.out.println("10 CENT");
                rueckgabewert -= 0.1;
            }
            if (rueckgabewert >= 0.05) {
                // 5 CENT-Muenzen
                System.out.println("5 CENT");
                rueckgabewert -= 0.05;
            }
        }
    }
}
